import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

var store = new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    access_token: '',
    role: '',
    userId: ''
  },
  mutations: {
    SET_TOKEN (state, data) {
      state.access_token = data
    },
    SET_ROLE (state, data) {
      state.role = data.role
      state.userId = data.userId
    },
    CLEAR_STATE (state) {
      state.access_token = ''
      state.role = ''
      state.userId = ''
    }
  },
  getters: {
    getUserId: state => () => state.userId
  }
})

export default store
