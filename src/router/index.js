import Vue from 'vue'
import Router from 'vue-router'
import SignIn from '@/components/SignIn'
import Patients from '@/components/Patients'
import PatientDetail from '@/components/PatientDetail'
import EditAppointment from '@/components/EditAppointment'
import ScheduleAppointment from '@/components/ScheduleAppointment'
import SignOut from '@/components/SignOut'
import NotFound from '@/components/NotFound'
import store from '@/store/account'

Vue.use(Router)

function blockPatientAccess (to, from, next) {
  if (store.state.role === 'Patient') {
    next({
      path: '/notfound'
    })
  } else {
    next()
  }
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/signout',
      name: 'SignOut',
      component: SignOut
    },
    {
      path: '/patients',
      name: 'Patients',
      component: Patients,
      beforeEnter: blockPatientAccess
    },
    {
      path: '/patient/:id',
      name: 'PatientDetail',
      component: PatientDetail
    },
    {
      path: '/editappointment/:id',
      name: 'EditAppointment',
      component: EditAppointment
    },
    {
      path: '/schedulenew/:patientId/:doctorId',
      name: 'ScheduleAppointment',
      component: ScheduleAppointment
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound
    }
  ]
})
