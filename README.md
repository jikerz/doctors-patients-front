# Front-end and API projects wiki

Please see this wiki page for my notes on installation and running the projects:

[https://bitbucket.org/jikerz/doctors-patients-api/wiki/Installation%20and%20running%20the%20project](https://bitbucket.org/jikerz/doctors-patients-api/wiki/Installation%20and%20running%20the%20project)

Please see the wiki page for my notes on the front-end project:

[https://bitbucket.org/jikerz/doctors-patients-front/wiki/Home](https://bitbucket.org/jikerz/doctors-patients-front/wiki/Home)

Please see the wiki page for my notes on the back-end API project:

[https://bitbucket.org/jikerz/doctors-patients-api/wiki/Home](https://bitbucket.org/jikerz/doctors-patients-api/wiki/Home)

---

# doctors-patients-front

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).